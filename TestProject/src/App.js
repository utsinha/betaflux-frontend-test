import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom'

// ==== All the pages Imported ====
import Template1 from './components/Template1'
import Template2 from './components/Template2'
import Template3 from './components/Template3'

let App = (props)=>{
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Template1}/>
                <Route exact path="/template-2" component={Template2}/>
                <Route exact path="/template-3" component={Template3}/>
            </Switch>
        </BrowserRouter>
    )
}

export default App;