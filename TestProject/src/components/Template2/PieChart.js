import React, {useState, useEffect} from 'react';
import {Pie} from 'react-chartjs-2';

const PieChart = (props) => {

    // ==== Hook for Chart Data ====
    const [chartData, setChartData] = useState([{
        label : "Default",
        weightage : 1,
        bg : "#eee"
    }])

    // ==== Hook for Loading State ====
    const [isLoading, setIsLoading] = useState(true)

    // ==== Use State hook to update data ====
    useEffect(()=>{

        let chartDataFromProps = props.data.map(item => {
            return{
                label : item.type,
                weightage : item.value,
                bg : item.shade
            }
        })

        setChartData(chartDataFromProps)
        setIsLoading(false)
    }, isLoading)

    // ==== Computing Chart data ====
    let pieChartData = {

        labels : chartData.map((item) => { return item.label}),
        datasets : [{
            data : chartData.map((item) => { return item.weightage}),
            backgroundColor : chartData.map((item) => { return item.bg})
        }],
        options : {
            legend : false,
            responsive: true,
            maintainAspectRatio: false
        }
    }

    // ==== Chart Options ====
    const options = {

        legend: {
            display: false
         },
         tooltips: {
            enabled: false
         }
    }

    return (
        <div className="chart-card__graph-wrapper">
            <Pie data={pieChartData} options={options}/>
        </div>
    );
};

export default PieChart;