import React, {useState, useEffect}from 'react';

// ==== Components Import =====
import Header from '../Shared/Header'
import DasboardWrapper from '../Shared/DasboardWrapper'
import ChartCard from './ChartCard'
import ListCard from '../Shared/ListCard'
import NextButton from '../Shared/NextButton'

const Template2 = (props) => {

    const [itemList, setItemList] = useState([
        {
            name : "Hamburgers",
            quantity : 2763.239
        },
        {
            name : "Tuna Sandwitch",
            quantity : 2763.239
        },
        {
            name : "Falafel with Pita Bread",
            quantity : 2763.239
        },{
            name : "Walnut Apple Pie",
            quantity : 2763.239
        }
    ])

    // ==== Scroll to top ===
    useEffect(()=>{
        window.scrollTo(0,0)
    }, [])

    let onNextButtonClick = ()=> props.history.push('/template-3');

    return (
        <React.Fragment>

            <Header goTo={"/"}/>

            <DasboardWrapper>

                <div className="content-row clearfix">

                    {/* ==== Chart Card ==== */}
                    <ChartCard/>

                    {/* ==== List Card ==== */}
                    <ListCard heading="Some Table" list={itemList}/>

                </div>

                <div className="content-row">
                    <div className="content-col float-right">

                        <NextButton handleClick={onNextButtonClick}/>

                    </div>
                </div>

            </DasboardWrapper>

        </React.Fragment>
    );
};

export default Template2;