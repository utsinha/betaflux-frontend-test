import React, {useState} from 'react';

import PieChart from './PieChart'

const ChartCard = () => {

    // ==== Chart assets data hook ====
    const [assets, setAssets] = useState([
        {
            type : "Net Investment",
            value : 16.03,
            shade : "#852B7F"
        },
        {
            type : "Net Earnings",
            value : 83.97,
            shade : "#E9383F"
        }
    ])

    return (
        <div className="content-col content-col--chart-card">
            <div className="card chart-card">
                <div className="chart-card__canvas">
                    <PieChart data={assets}/>
                </div>
                <div className="chart-card__content">
                    <h2 className="chart-card__title">
                        Pie Chart
                    </h2>
                    <p className="chart-card__paragraph">
                        Stew and rum will take you to places you never expected not to visit!
                    </p>
                    <ul className="chart-card__assets">

                        {assets && assets.map((asset, i)=>{

                            return (<li key={i} className="item">
                                        <span className="item__icon" style={{background : asset.shade}}/>
                                        <span className="item__data">{`${asset.type} : ₹ ${asset.value} Lacs`}</span>
                                    </li>)
                        })}
                    </ul>
                </div>
            </div>
        </div>

    );
};

export default ChartCard;