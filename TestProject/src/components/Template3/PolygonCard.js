import React from 'react';
import PolygonArt from '../../assets/images/asset_2.png'

const PolygonCard = () => {
    return (
        <div className="content-col content-col--polygon-card">
            <div className="polygon-img-box">
                <img src={PolygonArt} alt="Polygon Art" />
            </div>
        </div>

    );
};

export default PolygonCard;