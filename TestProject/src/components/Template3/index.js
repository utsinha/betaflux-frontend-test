import React, {useState, useEffect}from 'react';

// ==== Components Import =====
import Header from '../Shared/Header'
import DasboardWrapper from '../Shared/DasboardWrapper'
import ListCard from '../Shared/ListCard'
import PolygonCard from './PolygonCard'

const Template3 = (props) => {

    // ==== Template item list hook ====

    const [itemList, setItemList] = useState([
        {
            name : "Hamburgers",
            quantity : 2763.239
        },
        {
            name : "Tuna Sandwitch",
            quantity : 2763.239
        },
        {
            name : "Falafel with Pita Bread",
            quantity : 2763.239
        },{
            name : "Walnut Apple Pie",
            quantity : 2763.239
        }
    ])

    // ==== Scroll to top ===
    useEffect(()=>{
        window.scrollTo(0,0)
    }, [])

    return (
        <React.Fragment>

            <Header goTo={"/"}/>

            <DasboardWrapper>

                <div className="content-row clearfix">

                    {/* ==== PolygonCard Card ==== */}
                    <PolygonCard/>

                    {/* ==== List Card ==== */}
                    <ListCard heading="Another Table" list={itemList}/>

                </div>

            </DasboardWrapper>

        </React.Fragment>
    );
};

export default Template3;