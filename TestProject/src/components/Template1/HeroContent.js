import React from 'react';
import LandingDoodle from '../../assets/images/asset_1.png';
import {Link} from 'react-router-dom';

import ActionField from './ActionField'

const HeroContent = () => {

    return (
        <main className="hero-bg">
            <div className="container-fluid">
                <div className="hero-container">
                    <div className="hero-doodle">
                        <img src={LandingDoodle} alt="Landing Doodle" />
                    </div>
                    <Link to="/" className="hero-container__link">
                        <h1 className="hero-container__brand">
                            FOOBAR
                        </h1>
                    </Link>
                    <div className="hero-container__content">
                    <div className="hero-container__title">
                        Hello World
                    </div>
                    <div className="hero-container__subtitle">
                        Stew and rum will take you to places you never expected not to visit!
                    </div>
                    {/* <div className="hero-container__action-field">
                        <input type="text" />
                        <button>Go</button>
                    </div> */}
                    <ActionField/>
                    </div>
                </div>
            </div>
        </main>
    );
};

export default HeroContent;