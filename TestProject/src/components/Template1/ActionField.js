import React, {useState, useRef} from 'react';
import {withRouter} from 'react-router-dom'

const ActionField = (props) => {

    // ==== Name input data hook ====
    const [name, setName] = useState("")
    const nameInputRef = useRef(null)

    // ==== Regex to check for name ====
    let nameRegex = /^[a-z A-Z,.'-]+$/

    // ==== Handle input data change ====
    let handleChange = (event)=>{

        const {value} = event.target

        // ==== Simple name velidation ====

        if(value.match(nameRegex) || value === "")
            setName(value)
    }

    // ==== Handle Go button click ====
    let handleSubmit = () => {

        // ==== Check for name ====
        if(name.length > 0)
            props.history.push('/template-2')

        else
            nameInputRef.current.placeholder = "Your name first!"

    }

    return (
        <div className="hero-container__action-field">
            <input type="text" placeholder="Your Name" value={name} name="name" onChange={handleChange} ref={nameInputRef}/>
            <button onClick={handleSubmit}>Go</button>
        </div>
    );
};

export default withRouter(ActionField);