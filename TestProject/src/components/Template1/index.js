import React from 'react';
import Footer from './Footer'
import HeroContent from './HeroContent'

const Template1 = () => {

    return (
        <React.Fragment>
            <HeroContent/>
            <Footer/>
        </React.Fragment>

    );
};

export default Template1;