import React from 'react';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="container-fluid">
                <div className="footer__brand-watermark">
                    This is a react project
                </div>
            </div>
        </footer>
    );
};

export default Footer;