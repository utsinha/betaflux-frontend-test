import React from "react";

const ListCard = (props) => {

	const { heading, list} = props

	return (
		<div className="content-col content-col--list-card">
			<div className="card list-card">
				<h2 className="list-card__title">{heading || "Table"}</h2>
				<ul className="list-card__table">

					{list && list.map((item,i)=>{

						return (<li key={i} className="item">
									<div className="name">{item.name}</div>
									<div className="value">{item.quantity}</div>
								</li>)
					})}

				</ul>
			</div>
		</div>
	);
};

export default ListCard;
