import React from 'react';
import ArrowRight from '../../assets/images/asset_3.png'

const NextButton = (props) => {

    const {handleClick} = props

    return (
        <button className="action-btn" onClick={handleClick}>
            <span className="txt">Next Page</span>
            <img src={ArrowRight} alt="Next"/>
        </button>
    );
};

export default NextButton;