import React from 'react';
import {Link} from 'react-router-dom'

const Header = (props) => {

    const {goTo} = props
    return (
        <header className="header">
            <div className="container-fluid">
                <Link to={goTo} className="header__link">
                    <h1 className="header__brand">
                        FOOBAR
                    </h1>
                </Link>
            </div>
        </header>
    );
};

export default Header;