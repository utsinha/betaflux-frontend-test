import React from 'react';

const DasboardWrapper = (props) => {
    return (
        <main className="container-fluid main-content">
            {props.children}
        </main>
    );
};

export default DasboardWrapper;