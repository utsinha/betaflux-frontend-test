


## React Single Page Application Frontend Test

This test is designed to demonstrate your ability to successfully build a Single page React application and deliver a build closest possible to the design handover.

---

## Objective

To Build a single page react app. Please setup the project using the official 'Create React app' method.


## Design Handover

You have been provided with three templates in the PNG Format

1.	'Page_1.png' : Template 1
2.	'Page_2.png': Template 2
3.	'Page_3.png': Template 3


and three static assets to be used in the builds

1.	'asset_1.png' : (used in Template 1)
2.	'asset_2.png' : (used in Template 3)
3.	'asset_3.png' : (used in Template 2)

**Font Styes :**

1. Roboto Black
2. Roboto Light

**Colour Hex codes :**

1. Purple : #852B7F
2. Red : #E9383F
3. Gradient : #852B7F, #7D2B85, #E9383F
4. Black : #000000
5. Gray (background) : #EEEEEE
6. White: #FFFFFF

**Charts:**

Use chartJS or Highcharts for the pie chart on Template 2.

## User Experience

1. Template 1 should open on launch of Application.
2. Template 1 will allow user to enter their name and click the 'Go' button.
3. Template 2 is rendered on click of the 'Go' button, present on Template 1.
4. Template 3 is rendered on click of the 'Next Page' button, present on Template 2.

## Instructions

1. Clone repository :
	`git clone https://utsinha@bitbucket.org/utsinha/betaflux-frontend-test.git`

2. Add remote origin in your local machine : 
	`git remote add origin https://utsinha@bitbucket.org/utsinha/betaflux-frontend-test.git`

3. Create React Application named 'TestProject' : 
	`npm create-react-app TestProject`
	
4. Build the single page React application with templates - 1, 2 and 3

5. Commit your changes

6. Push your changes to the remote master branch
